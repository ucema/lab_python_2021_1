# Texto: Alfanumerico o String. Encerrado entre '' o  ""

print('Hola')
print("Hola")

print("33")

# Numericos: enteros: int     (Integer)
#            reales:  float   (Float: punto flotante)

print(33)

num1 = 15   # Almacenando el numero 15 en la variable num1
b = 3       # el = es el operador de asignacion.
nombre = "Python"
c = 3.1415

print(nombre)
print('El valor de PI es:', c, 'La variable nombre contiene', nombre)

# Operadores aritmeticos
# +   sumar
# -   restar
# *   multiplicar
# **  potencia              2 ** 3 = 8
# /   Division real         5 / 3 = 1.66666666666
# //  Division entera       5 // 3 = 1
# %   Resto de la division entera o modulo   5 % 3 = 2
a = 14
b = 3

n1 = a / b
print(n1)

n2 = a // b
print(n2)

n3 = a * 5
print(n3)

n4 = a % 5
print(n4)

n5 = 2 ** 4
print(n5)


# Operadores Logicos: Verdadero o Falso (True , False)
# Boolean: True/False
# and      (Y, conjuncion)
# or       (O, disyuncion)
# not      (no, negacion)

# p => q  es lo mismo que p o no q

p = True
q = False
r = p and q
print(r)

print(12 * 6)
print(p or q)

n = '-'
print(n * 50)

print(type(n1))
