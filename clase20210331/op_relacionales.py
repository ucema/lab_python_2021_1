# Operadores relacionales (Comparacion): Boolean
# <   >   <=   >=    
# ==    Igualdad
# !=    Distinto

numero1 = int(input('Ingrese un numero: '))
numero2 = int(input('Ingrese un numero: '))

print(numero1 < numero2)
print(numero1 == numero2)
print(numero1 != numero2)
print(numero1 > numero2)

r = numero1<numero2 or numero1==numero2     # numero1 <= numero2
print(r)
