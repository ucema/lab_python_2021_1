'''
comentario
de 
muchas lineas
'''
print('Comienza mi super programa!!!')

mi_variable = input('Ingrese algo:')   # el input sirve para leer desde teclado
print('Usted ingreso: ', mi_variable)       # Todo lo ingresado desde teclado es TEXTO o String

# Hacer un programa que pida 2 numeros y muestre la multiplicacion
numero1 = int(input('Ingrese primer numero:'))    #      str --- convertirlo --> int
numero2 = int(input('Ingrese segundo numero:'))
resultado = numero1 * numero2
print('La multiplicacion es:', resultado)

x = int("123")
print(x*2)

'''
f(x) = x + 1
g(x) = x * 5
f(g(x)) = g(x) + 1 = (x*5) + 1

f(g(2)) = f(10) = 10 + 1 = 11
g(2) = 2*5 = 10
'''
print('-' * 50)

# Hacer un programa que dada la cotizacion del dolar y cuantos pesos se tienen, cuantos dolares puedo comprar.
pesos = float(input('Ingrese pesos: '))
cotizacion = float(input('Ingrese cotizacion: '))
dolares = pesos / cotizacion
print('Se pueden comprar: ', dolares, ' con esos pesos.')





