# JSON: JavaScript Object Notation
# HTTP / HTTPS --- Hyper TEXT Transfer Protocol  (Secure)
'''
Navegador   ----> Hace peticion  ----->  Servidor
  javascript            TEXTO
'''

import json

t = '''
{
  "nombre": "Gabriel",
  "apellido": "Barrera",
  "peliculas_favoritas": ["Pelicula1", "Pelicula2"]
}
'''

obj = json.loads(t)

print(obj)
print(obj['apellido'])

class Persona():
    nombre = ''
    edad = 0

p = Persona()
p.nombre = 'Pipi'
p.edad = 20

print(p.__dict__)

r = json.dumps(p.__dict__)
print('En JSON queda: ', r)
print(type(r))

