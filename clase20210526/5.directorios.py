import os

directorio_actual = os.getcwd()
print('Esta en el directorio: ', directorio_actual)


archivos = os.listdir()
print(archivos)


archivos = os.listdir('D:\\projects\\ucema\\lab_python_2021_1')
print(archivos)

os.mkdir('ejemplo1')

os.chdir('ejemplo1')

directorio_actual = os.getcwd()
print('Esta en el directorio: ', directorio_actual)

os.chdir('..')
directorio_actual = os.getcwd()
print('Esta en el directorio: ', directorio_actual)

os.rename('ejemplo1', 'ejemplo2')

archivos = os.listdir()
print(archivos)

os.rmdir('ejemplo2')    # Solo elimina el directorio, si no contiene nada

archivos = os.listdir()
print(archivos)

arch = open('eliminar.txt', 'w')
arch.write('me borran')
arch.close()
archivos = os.listdir()
print(archivos)

os.remove('eliminar.txt')
archivos = os.listdir()
print(archivos)