from functools import reduce

# Modo de acceso al archivo
# w: Escritura
# r: Lectura
# a: Agregar
# x: Escritura 

# Mode de tipo de archivo
# t: texto  (default)
# b: binario

lineas = ['ccccc', 'ddddd']
numeros = [10, 20, 30]

arch1 = open('archivo1.txt', 'w')
arch1.write('Linea numero 1\n')
arch1.write('Linea numero 2\n')
arch1.write('Linea numero 3\n')
arch1.write('aaaaaa\nbbbbb\n')

arch1.writelines(map(lambda linea: linea + '\n', lineas))
arch1.writelines(map(lambda numero: str(numero) + '\n', numeros))
arch1.write(reduce(lambda res, elem: str(res) + ',' + str(elem) , numeros))
arch1.write('\n')
arch1.close()

arch2 = open('archivo1.txt', 'r')
# contenido = arch2.readlines()   # Devuelve una lista de strings con las lineas del archivo
contenido = arch2.read()    # Devuelve todo el contenido del archivo
print(contenido)
arch2.close()

print('-' * 50)

arch2 = open('archivo1.txt', 'r')
for linea in arch2:
    print(linea, end='')

arch2.close()

print()
print('-' * 50)

arch2 = open('archivo1.txt', 'r')
contenido = arch2.readline(4)
print(contenido)
contenido = arch2.readline(8)
print(contenido)
contenido = arch2.readline(5)
print(contenido)
arch2.close()


print()
print('-' * 50)

arch2 = open('archivo1.txt', 'r')
contenido = arch2.readline()
while contenido != '':
    print(contenido, end='')
    contenido = arch2.readline()

arch2.close()


arch3 = open('archivo1.txt', 'a')
arch3.write('nueva linea agregada 1\n')
arch3.write('nueva linea agregada 2\n')
arch3.close()


print('-' * 50)

arch2 = open('archivo1.txt', 'r')
for linea in arch2:
    print(linea, end='')

arch2.close()


'''
Apellido
Nombres
Edad
Domicilio
Apellido
Nombres
Edad
Domicilio
Apellido
Nombres
Edad
Domicilio
Apellido
Nombres
Edad
Domicilio




'''