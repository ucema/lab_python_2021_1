# CSV: Comma Separeted Values

arch = open('padron.csv')

for linea in arch:
    campos = linea.split(';')
    # 25082020;01092020;30092020;20000182770;D;S;N;0,00;0,00;00;00;
    print(f'La CUIT es {campos[3]} y el porcentaje es {campos[7]}')

    if ',' in campos[7]:
        alicuota = float(campos[7].replace(',', '.'))
    else:
        alicuota = float(campos[7])

    print(alicuota * 100)

arch.close()
