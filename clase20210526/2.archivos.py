from functools import reduce


class Factura():
    numero = 0
    fecha = ''
    nombre_cliente = ''
    cantidades = []
    precios = []

    def __init__(self, numero=0, fecha='', nombre_cliente='CF', cantidades=[], precios=[]):
        self.numero = numero
        self.fecha = fecha
        self.nombre_cliente = nombre_cliente
        self.cantidades = cantidades
        self.precios = precios

def listar_facturas(lista):
    for fc in lista:
        print(fc.numero, '\t', fc.fecha, '\t', fc.cantidades, '\t', fc.precios)

f1 = Factura(1, '20200901', 'Pepe', [2, 1, 2], [12.4, 35.1, 21.5])
f2 = Factura(2, '20200907', 'Pipi', [1, 1], [35.1, 21.5])
f3 = Factura(3, '20200824', 'Pepo', [10, 5], [20.99, 49.99])

fcs = [f1, f2, f3]

archivo1 = open('facturas.txt', 'w')

for factura in fcs:
    archivo1.write(str(factura.numero) + '\n')
    archivo1.write(factura.fecha + '\n')
    archivo1.write(factura.nombre_cliente + '\n')
    archivo1.write(reduce(lambda res, elem: str(res) + ';' + str(elem) , factura.cantidades) + '\n')
    archivo1.write(reduce(lambda res, elem: str(res) + ';' + str(elem) , factura.precios) + '\n')

archivo1.close()
