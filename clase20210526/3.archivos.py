
class Factura():
    numero = 0
    fecha = ''
    nombre_cliente = ''
    cantidades = []
    precios = []

    def __init__(self, numero=0, fecha='', nombre_cliente='CF', cantidades=[], precios=[]):
        self.numero = numero
        self.fecha = fecha
        self.nombre_cliente = nombre_cliente
        self.cantidades = cantidades
        self.precios = precios

def listar_facturas(lista):
    for fc in lista:
        print(fc.numero, '\t', fc.fecha, '\t', fc.nombre_cliente, '\t', fc.cantidades, '\t', fc.precios)


facturas = []


archivo = open('facturas.txt', 'r')
numero_factura = archivo.readline()
while numero_factura != '':
    fecha_factura = archivo.readline().replace('\n', '')
    nombre_cliente = archivo.readline().replace('\n', '')
    cantidades = list(map(lambda e: int(e), archivo.readline().split(';')))
    precios = list(map(lambda e: float(e), archivo.readline().split(';')))

    factura = Factura(
      int(numero_factura),
      fecha_factura,
      nombre_cliente,
      cantidades,
      precios
    )

    facturas.append(factura)

    numero_factura = archivo.readline()


listar_facturas(facturas)



archivo.close()