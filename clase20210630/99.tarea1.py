'''
https://swapi.dev/api/

https://swapi.dev/api/people
https://swapi.dev/api/planets

Armar un programa que genere una lista con los personajes (Objetos) y que contenga 
su mundo de origen.

https://swapi.dev/api/people/1

'''
import requests


class Character():
  id = 0
  name = ''
  gender = ''
  homeworld = None

  def __init__(self, id, name, gender, homeworld):
    self.id = id
    self.name = name
    self.gender = gender
    self.homeworld = homeworld

  def __str__(self):
    return f'{self.name}, {self.gender}, {self.homeworld}'


class Planet():
  id = 0
  name = ''
  terrain = ''

  def __init__(self, id, name, terrain):
    self.id = id
    self.name = name
    self.terrain = terrain

  def __str__(self):
    return f'{self.id}, {self.name}, {self.terrain}'


# Cargando los planetas
planetas = []
personajes = []

respuesta = requests.get('https://swapi.dev/api/planets')
datos = respuesta.json()
cantidad = 0
total = datos['count']

while cantidad < total:
  url_siguientes = datos['next']
  resultados = datos['results']

  for p in resultados:
    cantidad += 1
    planetas.append(Planet(
      p['url'],
      p['name'],
      p['terrain']
    ))
  
  if url_siguientes != None:
    respuesta = requests.get(url_siguientes)
    datos = respuesta.json()



respuesta = requests.get('https://swapi.dev/api/people')
datos = respuesta.json()
cantidad = 0
total = datos['count']

while cantidad < total:
  url_siguientes = datos['next']
  resultados = datos['results']

  for p in resultados:
    cantidad += 1
    personajes.append(Character(
      p['url'],
      p['name'],
      p['gender'],
      list(filter(lambda pla: pla.id == p['homeworld'], planetas))[0]
    ))
  
  if url_siguientes != None:
    respuesta = requests.get(url_siguientes)
    datos = respuesta.json()


for p in personajes:
  print(p)

