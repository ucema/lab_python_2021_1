'''
HTTP / HTTPS  ----> tranferencia de texto

REST: CRUD o ABM
- POST: Creacion
- PUT: Modificacion (Se pasa todo el recurso completo)
- PATCH: Modificacion (Solo se indican los campos/partes modificados)

- GET: Obtener un recurso
- DELETE: Eliminacion

- ACTIONS: Acciones disponibles

curl  https://curl.haxx.se/windows/

postman

https://httpbin.org

API
Endpoint

www.google.com?q=python3&usr=sarasa&pass=1234

url friendly
www.banco.com.ar/productos/paquetes/cuentasueldo

respuesta es Response
1xx Informaciones
2xx Correcto
3xx Redireccionamientos
4xx: Errores de request
5xx: Error del servidor

Projecto. Librerias.

requests  3.41.58    (mayor-menor-revision)

virtualenv -- Entornos virtuales de python
  - python
  - pip (Administrador de paquetes)

Crear:
  virtualenv env
  o
  virtualenv env -p python3

Activar:
  Linux/mac:        source env/bin/activate

  Windows (bash):   source env/Scripts/activate

  Windows (cmd/PS): cd env/Script
                    activate.bat

Desactivar:
  deactivate


Instalar paquete:
  pip install requests

Ver paquetes instalados:
  pip freeze

  pip freeze > requeriments.txt

Instalar paquetes:
 pip install -r requeriments.txt 



wheel (whl)

fiddler   http/s
wire shark

'''
import requests

respuesta = requests.get('https://httpbin.org/get?param=1234&param2=xyz')     # Response

print(respuesta.status_code)
print(respuesta.content)    # byte-array
print(respuesta.text)
print(respuesta.json())

print('-' * 80)
d = respuesta.json()
print(d['args'])

print('-' * 80)

respuesta = requests.get(
    'https://httpbin.org/get',
    params={'param1': '12345', 'param2': 'abcd'}
  )

print(respuesta.json()['args'])

respuesta = requests.get(
  'https://api.github.com/search/repositories',
  params={'q': 'request+language:python'},
  headers={'Accept': 'application/vnd.github.v3.text-match+json'}
)
datos = respuesta.json()
print(datos['items'][0])

