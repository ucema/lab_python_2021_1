import requests, json, pickle


respuesta = requests.post(
  'https://httpbin.org/post',
  json={'clave1': 'valor1', 'clave2': 'valor2'}
)

print(respuesta.json())

print('*' * 80)

respuesta = requests.post(
  'https://httpbin.org/post',
  data={'clave1': 'valor1', 'clave2': 'valor2'}
)

print(respuesta.json())
print(respuesta.request.body)

print('*' * 80)


class Character():
  id = 0
  name = ''
  gender = ''
  mother = None

  def __init__(self, id, name, gender):
    self.id = id
    self.name = name
    self.gender = gender


luke = Character(1, 'Luke', 'Masc')
luke.mother = Character(3, 'Padme', 'Fem')

leia = Character(2, 'Leia', 'Fem')
ps = [luke, leia]


# respuesta = requests.post('https://httpbin.org/post', data=pickle.dumps([ p.__dict__ for p in ps ]))
# print(respuesta.json())


respuesta = requests.post('https://httpbin.org/post', json=[ p.__dict__ for p in ps ])
print(respuesta.json())

print('*' * 80)

respuesta = requests.post('https://httpbin.org/post', data=json.dumps([ p.__dict__ for p in ps ]))
print(respuesta.json())

