from flask import request, Response, Blueprint
import json
from . import productos
# from familias import bp_familias



bp_productos = Blueprint('productos', __name__)
# bp_productos.register_blueprint(bp_familias)


@bp_productos.route('/')
def listar_productos():
  datos = json.dumps(productos)
  return Response(datos, mimetype='application/json')


@bp_productos.route('/<familia>')
def listar_produtos_familia(familia):
  if not familia in productos:
    return Response('Familia inexistente', status=404)

  datos = json.dumps(productos[familia])
  return Response(datos, mimetype='application/json')

@bp_productos.route('/<familia>/<int:id>')
def leer_producto(familia, id):
  # id = int(id)

  if familia in productos:
    if id in productos[familia]:
      prod = productos[familia][id]

      return Response(prod)
  
    else:
      return Response('Producto inexistente', status=404)
  else:
    return Response('Familia inexistente', status=404)


@bp_productos.route('/', methods=['POST'])
def agregar_producto():
  # if request.method == 'POST':
  #   return 'Es un POST'

  if not 'nombre' in request.form:
    return Response('Falta nombre', status=400)

  if not 'familia' in request.form:
    return Response('Falta familia', status=400)

  familia = request.form['familia']
  nombre = request.form['nombre']

  if not familia in productos:
    productos[familia] = {}
    id = 1
  else:
    id = max(productos[familia].keys()) + 1

  productos[familia][id] = nombre
  
  return Response(str(id), status=201)

@bp_productos.route('/<familia>/<int:id>', methods=['DELETE'])
def eliminar_producto(familia, id):
  
  if not familia in productos:
    return Response('Familia incorrecta', status=404)

  if not id in productos[familia]:
    return Response('Id producto invalido', status=404)

  del productos[familia][id]

  return 'ok'


@bp_productos.route('/', methods=['PUT'])
def modificar_producto():

  if not 'id' in request.form:
    return Response('Falta id', status=400)

  if not 'nombre' in request.form:
    return Response('Falta nombre', status=400)

  if not 'familia' in request.form:
    return Response('Falta familia', status=400)

  id = int(request.form['id'])
  familia = request.form['familia']
  nombre = request.form['nombre']

  if not familia in productos:
    return Response('Familia incorrecta', status=404)

  if not id in productos[familia]:
    return Response('Id producto invalido', status=404)

  # El Update del producto
  productos[familia][id] = nombre

  return Response(str(id), status=202)
