from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float, ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, relationship, backref


Base  = declarative_base()

engine = create_engine('sqlite:///base_rel.sqlite', echo=True) # String de conexion

Session = sessionmaker()
Session.configure(bind=engine)


class Familia(Base):
  __tablename__ = 'familia'
  id = Column(Integer, primary_key=True)
  descripcion = Column(String)

  def __str__(self):
    return '{}-{}'.format(self.id, self.descripcion)


class Producto(Base):
  __tablename__ = 'producto'
  id = Column(Integer, primary_key=True)
  descripcion = Column(String)
  precio = Column(Float)
  peso = Column(Float)
  familia_id = Column(Integer, ForeignKey('familia.id'))     # nombre de tabla
  familia = relationship(
    Familia,
    backref=backref('productos', uselist=True, cascade='delete,all')
  )

  def __str__(self):
    return '{}-{}'.format(self.id, self.descripcion)


Base.metadata.create_all(engine)


# *****************************************************

# f = Familia()
# f.descripcion = 'Juguetes'

# a = Producto()
# a.descripcion = 'yerba'
# a.precio = 250.99
# a.peso = 1000
# a.familia = f


# s = Session()
# s.add(f)
# s.add(a)
# s.commit()

# *****************************************************


# s = Session()
# familia = s.query(Familia).filter_by(descripcion='Almacen')[0]

# a = Producto()
# a.descripcion = 'aceite'
# a.precio = 190.50
# a.familia = familia
# a.peso = 1000

# s.add(a)
# s.commit()



# *****************************************************

s = Session()
familia = s.query(Familia).filter_by(descripcion='Almacen')[0]

for p in familia.productos:
  print(p)

