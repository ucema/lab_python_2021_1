from flask import Flask, Response, request
import json

app = Flask(__name__)
app.debug = True


'''
param1: tiene que ser obligatorio
param2: no obligatorio, valor por default
'''

@app.route('/')
def inicio():
  print(request.args)

  if not 'param1' in request.args:
    return Response('Falta param1', status=400)

  print(request.args['param1'])
  
  print(request.args.get('param2', 0))

  return 'Hola!!\n'


productos = {
  'frutas': {1: 'Manzana', 2: 'Pera', 3: 'Anana' },
  'verduras': {1: 'Acelga', 2: 'Papa', 3: 'Lechuga', 4: 'Batata' }
}


@app.route('/productos')
def listar_productos():
  datos = json.dumps(productos)
  return Response(datos, mimetype='application/json')


@app.route('/productos/<familia>')
def listar_produtos_familia(familia):
  if not familia in productos:
    return Response('Familia inexistente', status=404)

  datos = json.dumps(productos[familia])
  return Response(datos, mimetype='application/json')

@app.route('/productos/<familia>/<int:id>')
def leer_producto(familia, id):
  # id = int(id)

  if familia in productos:
    if id in productos[familia]:
      prod = productos[familia][id]

      return Response(prod)
  
    else:
      return Response('Producto inexistente', status=404)
  else:
    return Response('Familia inexistente', status=404)


@app.route('/productos', methods=['POST'])
def agregar_producto():
  # if request.method == 'POST':
  #   return 'Es un POST'

  if not 'nombre' in request.form:
    return Response('Falta nombre', status=400)

  if not 'familia' in request.form:
    return Response('Falta familia', status=400)

  familia = request.form['familia']
  nombre = request.form['nombre']

  if not familia in productos:
    productos[familia] = {}
    id = 1
  else:
    id = max(productos[familia].keys()) + 1

  productos[familia][id] = nombre
  
  return Response(str(id), status=201)

@app.route('/productos/<familia>/<int:id>', methods=['DELETE'])
def eliminar_producto(familia, id):
  
  if not familia in productos:
    return Response('Familia incorrecta', status=404)

  if not id in productos[familia]:
    return Response('Id producto invalido', status=404)

  del productos[familia][id]

  return 'ok'


@app.route('/productos', methods=['PUT'])
def modificar_producto():

  if not 'id' in request.form:
    return Response('Falta id', status=400)

  if not 'nombre' in request.form:
    return Response('Falta nombre', status=400)

  if not 'familia' in request.form:
    return Response('Falta familia', status=400)

  id = int(request.form['id'])
  familia = request.form['familia']
  nombre = request.form['nombre']

  if not familia in productos:
    return Response('Familia incorrecta', status=404)

  if not id in productos[familia]:
    return Response('Id producto invalido', status=404)

  # El Update del producto
  productos[familia][id] = nombre

  return Response(str(id), status=202)



@app.route('/test/<string:id>')
def test_str(id):
  print(id)
  return 'str\n'


@app.route('/test/<int:id>')
def test_int(id):
  print(id)
  return 'int\n'





if __name__ == '__main__':
  app.run()



