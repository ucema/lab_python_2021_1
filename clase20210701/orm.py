# sqlalchemy

# SQlServer, Mysql, postgres, oracle, sybase, sqlite, access, etc.

# SQLServer: SELECT TOP 10 * FROM Clientes 
# MySQL:     SELECT * FROM Clientes LIMIT 10
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


Base  = declarative_base()

engine = create_engine('sqlite:///mi_base.sqlite', echo=True) # String de conexion

Session = sessionmaker()
Session.configure(bind=engine)


class Producto(Base):
  __tablename__ = 'producto'
  id = Column(Integer, primary_key=True)
  descripcion = Column(String)
  precio = Column(Float)
  peso = Column(Float)
  familia = Column(String)

  def __str__(self):
    return '{}-{}'.format(self.id, self.descripcion)


class Familia(Base):
  __tablename__ = 'familia'
  id = Column(Integer, primary_key=True)
  descripcion = Column(String)

  def __str__(self):
    return '{}-{}'.format(self.id, self.descripcion)


# https://www.connectionstrings.com/

Base.metadata.create_all(engine)



a = Producto()
# a.id = 10
a.descripcion = 'yerba'
a.precio = 250.99
a.familia = 'Almacen'
a.peso = 1000

f = Familia()
f.descripcion = 'Almacen'


s = Session()   # Transaccion
s.add(a)
s.add(f)
s.commit()


productos = s.query(Producto)

for p in productos:
  print(p)

print('')

menores_10 = s.query(Producto).filter(Producto.id < 10)

for p in menores_10:
  print(p)



print('')

primeros = s.query(Producto)[:3]
for p in primeros:
  print(p)


# print('')
# print('Eliminar')

# pr = s.query(Producto).get(2)
# s.delete(pr)
# s.commit()


# print('')

# menores_10 = s.query(Producto).filter(Producto.id < 10)

# for p in menores_10:
#   print(p)


print('')

print('Update')

pr = s.query(Producto).get(1)
pr.descripcion = 'Azucar'
s.commit()



