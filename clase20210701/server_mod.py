from flask import Flask
from modelo.producto import bp_productos


app = Flask(__name__)
app.debug = True

app.register_blueprint(bp_productos, url_prefix='/productos')


if __name__ == '__main__':
  app.run()
  