
r1 = range(5)    # iterable

# range(n) devuelve numeros entre 0 y n-1

for n in r1:
    print(n, end='\t')      # \n es salto de linea     \t tabulacion

print()
print('-' * 50)

print('range(10):')
for n in range(10):
    print(n, end='\t')

print()
print('-' * 50)

print('range(3, 10):')
for n in range(3, 10):        # [3, 10)
    print(n, end='\t')

print()
print('-' * 50)

print('range(3, 20, 2):')
for n in range(3, 20, 2):
    print(n, end='\t')

    
print()
print('-' * 50)

print('range(20, 3, -2):')
for n in range(20, 3, -2):
    print(n, end='\t')

print()
print('-' * 50)
r2 = list(range(5))
print(r2)
