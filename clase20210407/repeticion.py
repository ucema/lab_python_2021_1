# Control + C : corta la ejecucion del programa.

i = 0
while i < 5:
    print('Soy Leyenda')

    i = i + 1

print('-' * 50)

'''
inicializacion de las variables en la condicion

while condicion:
    pedir resto de datos

    instrucciones/procesar

    actualizacion de variables en la condicion
'''

# Se realiza una encuesta, se cargan datos mientras la EDAD sea mayor a cero,
# se pide la EDAD y el NOMBRE. Al finalizar mostrar el nombre del mayor.

edad = int(input('Ingrese edad:'))
edad_mayor = edad

while edad > 0:
    nombre = input('Ingreso:')

    if edad >= edad_mayor:
        edad_mayor = edad
        nombre_mayor = nombre

    edad = int(input('Ingrese edad:'))

print('La edad mayor es', edad_mayor, ' de', nombre_mayor)
