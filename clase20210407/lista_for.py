digitos = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

for d in digitos:
    print(d)

animales = ['gato', 'canario', 'loro', 'pez', 'perro']
for animal in animales:
    print(animal)

numeros = [51, 56, 20, 48, -8, 14, 68]
suma_numeros = 0
for numero in numeros:
    print('Voy a sumar el numero ', numero)
    suma_numeros = suma_numeros + numero
    print()

print('La suma es', suma_numeros)
