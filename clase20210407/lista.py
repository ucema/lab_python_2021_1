# Colecciones
# - Listas (list)
# - Tuplas (tuple)
# - Diccionarios (dict)
#           0    1    2    3    4
vocales = ['a', 'e', 'i', 'o', 'u', 'y']
digitos = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

varios = ['uno', 1, 'pi', 3.14, True, 'que es esto', 666, digitos]

vocal_a = vocales[0]    # 0 es el indice de la lista.
vocal_b = vocales[1]

i = 0
while i < len(vocales):
    print(vocales[i])

    i = i + 1

print(varios)
varios[3] = 'no mas 3.14'
print(varios)

varios.append('soy nuevo')
print(varios)

varios.extend([10, 20, 30])
print(varios)

varios.insert(1, 'nuevo 1')
print(varios)

print('-' * 50)
removido = varios.pop(3)
print(varios)
print(removido)

varios.remove(666)
print(varios)

varios.append('uno')
print(varios)

varios.remove('uno')
print(varios)

print('-' * 50)

print(digitos)
digitos.reverse()
print(digitos)

print('-' * 50)

digitos.sort()
print(digitos)

digitos.sort(reverse=True)
print(digitos)
