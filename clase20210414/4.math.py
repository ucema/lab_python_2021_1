import math

raiz = math.sqrt(16)
print(raiz)

a = math.log(math.e)    # log natural:   ln()
print(a)

b = math.log(100, 10)   # log en base 10 de 100
print(b)

c = 10 ** 2   # 10 al cuadrado
print(c)

pi = math.pi
print(pi)

d = math.ceil(pi)
print(d)

e = math.floor(pi)
print(e)

x = 3.48
f = round(x)
print(f)
