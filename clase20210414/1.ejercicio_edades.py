'''
leer edades hasta edad 200
- mostrar cantidad de personas menores
- entre 21 y 40
- mayores a 40


hasta edad = 200   -->  mientras NOT edad = 200
'''

edad = int(input("Ingrese edad: "))

cantidad_menor = 0
cantidad_entre21y40 = 0
cantidad_mayor40 = 0

while not edad == 200:    # while edad != 200:

    if edad < 21:
        cantidad_menor += 1     # cantidad_menor = cantidad_menor + 1
    elif edad <= 40:
        cantidad_entre21y40 += 1
    else:
        cantidad_mayor40 += 1

    edad = int(input("Ingrese edad: "))

print('Cantidad menores: ', cantidad_menor)
print('Cantidad entre 21 y 40: ', cantidad_entre21y40)
print('Cantidad mayores a 40: ', cantidad_mayor40)



# if 0 < edad1 < edad2: 

# if 0 < edad1 and edad1 < edad2:
  