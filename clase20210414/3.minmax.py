import random

# numeros = random.randint(0, 5)  # 0 1 2 3 4 o 5 
# numeros = random.randrange(0, 5)  # 0 1 2 3 o 4

# random.seed(1234)

# for i in range(10):
#     numeros = random.randint(0, 5)
#     print(numeros)

numeros = random.sample(range(-1000, 1000), 15)
print(numeros)

maximo = numeros[0]
for n in numeros:
    if maximo < n:
        maximo = n

print('El maximo es: ', maximo)

maximo2 = max(numeros)
print('El maximo es: ', maximo2)

minimo = min(numeros)
print('El minimo es: ', minimo)

