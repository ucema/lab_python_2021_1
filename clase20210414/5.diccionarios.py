# Clave (key)  - valor (value)
'''
Key     Value
1       uno
2       dos
3       tres
4       cuatro
5       cinco
'''

diccionario = {
  1: 'uno',
  2: 'dos',
  3: 'tres',
  4: 'cuatro'
}

print(diccionario)

v = diccionario[2]
print(v)

diccionario2 = {
  'k1': 'uno',
  'k2': 'dos',
  'k3': [1,2,3,4,5],
  'k4': 'cuatro'
}

v = diccionario2['k3']
print(v)

print('*' * 50)

diccionario3 = {}
diccionario3['clave1'] = 'el valor a la clave1'
diccionario3['clave2'] = 'Key2 value'

print(diccionario3)

diccionario3['clave2'] = 'Key2 value modificado'
print(diccionario3)

print('*' * 50)

for e in diccionario2:    # Recorro las CLAVES
    print(e, diccionario2[e])

print('*' * 50)

for v in diccionario2.values():
    print(v)

print('*' * 50)

for k, v in diccionario2.items():
    print(k, v)

print('*' * 50)

e = diccionario2.pop('k2')
print(e)
print(diccionario2)

print('*' * 50)

l = len(diccionario2)
print('Longitud del dic: ', l)
