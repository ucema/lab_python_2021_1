#         0    1    2    3    4    5    6    7    8    9    10
lista = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k']
print('lista = \t', lista)

a = lista[3]
print('lista[3] = \t', a)

b = lista[4:]   # Desde el indice 4 al final
print('lista[4:] = \t', b)

c = lista[4:7]  # Desde el indice 4 hasta 7-1 ( o sea 6)
print('lista[4:7] = \t', c)

d = lista[:6]   # Desde el inicio hasta 6-1 (o sea 5) es equivalente a lista[0:6]
print('lista[:6] = \t', d)

e = lista[1:7:2]  # Desde la posicion 1 hasta la 7-1, "saltando" de a 2
print('lista[1:7:2] = \t', e)

f = lista[::2]
print('lista[::2] = \t', f)

g = lista[::-2]
print('lista[::-1] = \t', g)
