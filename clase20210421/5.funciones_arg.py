
def funcion_ejemplo(**param):
    print(param)
    for k, v in param.items():
        print(k, v)


def ejemplo_funcion_todo(param1, *param4, **param5):
    print(param1)
    # print(param2)
    # print(param3)
    print(param4)
    print(param5)


funcion_ejemplo(arga=1, argb=2, argc='cccc', argd=3.14)

print('-' * 50)
ejemplo_funcion_todo('uno', 1,2,3,4,5, arg1='two', arg2='onnn')