# En la tuplas no se pueden modificar los elementos (cambiar valor, agregar, o sacar)

# [] para listas
# {} para diccionarios
# () para tuplas

a = (10, 20, 30, 40)
print(a)

valor = a[3]
print(valor)

# a[2] = 44    No se puede

longitud = len(a)
print('Longitud de la tupla: ', longitud)

for e in a:
    print(e)

print('*' * 50)

mi_tupla = ('uno', 'dos', 'tres', 'cuatro')
mi_lista = list(mi_tupla)

print(mi_lista, type(mi_lista))
mi_lista.append('cinco')      # Agrega al final
mi_lista.insert(0, 'cero')    # Agrega en la posicion indicada, 0
print(mi_lista)

otra_tupla = tuple(mi_lista)
print(otra_tupla)

print()

print('*' * 50)

v1, v2, v3, v4 = a
print(v1)

print('*' * 50)

_, y, _ = ('uno', 'dos', 'tres')
print(y)