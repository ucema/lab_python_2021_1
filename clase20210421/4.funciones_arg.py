
# print()
# print('a')
# print('a', 'b')
# print('a', 'b', 'c')

def calcular_promedio(*numeros):
    print(numeros)

    return sum(numeros) / len(numeros)


def buscar(elemento, *numeros):
    if elemento in numeros:
        return True

    print('No esta')
    return False

def ejemplo_funcion(param1, *param4, param2='dos', param3='tres' ):
    print(param1)
    print(param2)
    print(param3)
    print(param4)


p = calcular_promedio(10, 5, 324, 65, 32, 657, 40)
print(p)

esta = buscar(65, 10, 5, 324, 65, 32, 657, 40)
print(esta)

ejemplo_funcion('uno', 1,2,3,4,5, param2='two')
