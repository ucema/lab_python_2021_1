# f(x) = 2x + 2
# f(3) ----> 8

# Definicion de funciones

def una_funcion():
    print('---------------')
    print('Soy una funcion')
    print('---------------')
    
# Recibe un numero
def f(x):
    resultado = 2 * x + 2
    return resultado

def sumar(lista):
    '''
    Suma los elementos de una lista de numeros

    :lista: Debe ser del tipo list
    :return: Un numero siendo la suma
    '''
    total = 0
    for elemento in lista:
        total = total + elemento

    return total

def calcular_estadisticas(lista):
    '''
    Calcula varias estadisticas de una lista de numeros

    :lista: Debe ser del tipo list
    :return: la suma, la longitud, el promedio
    '''
    suma = sum(lista)
    longitud = len(lista)
    promedio = suma / longitud

    return suma, longitud, promedio

def leer_numero_positivo():
    numero = int(input('Ingrese numero positivo:'))
    while not (numero >= 0):
        numero = int(input('Ingrese numero positivo:'))

    return numero

def sumar_multiplos(lista: list, divisor):
    total = 0
    for numero in lista:
        if numero % divisor == 0:
            total = total + numero

    return total

# La parte principal del programa

una_funcion()

y = f(3)
print(y)

mi_lista = [6, 7, 321, 36, 32]
total = sumar(mi_lista)
print(total)

a, _, c = calcular_estadisticas(mi_lista)
# print(t[0], '\n', t[1], '\n', t[2])
# print(t[1])
# print(t[2])
print('La suma es:', a)
print('El promedio es:', c)

x = leer_numero_positivo()

sm = sumar_multiplos(mi_lista, 6)
print(sm)