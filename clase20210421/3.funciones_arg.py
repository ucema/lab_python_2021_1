
def leer_numero_positivo(mensaje='Ingrese numero positivo:'):
    numero = int(input(mensaje))
    while not (numero >= 0):
        numero = int(input(mensaje))

    return numero

def funcion_ejemplo(param1, param2='hola', param3=100):
    print(param1)
    print(param2)
    print(param3)


n = leer_numero_positivo('Ingrese el precio del auto:')

n = leer_numero_positivo()

print('-' * 50)
funcion_ejemplo(10, 20)

print('-' * 50)
funcion_ejemplo(10, param3=30)

