# Lab. Programacion Python 2021-1C

## Clase 2021-03-17
  - 

## Clase 2021-03-24
  - Feriado

## Clase 2021-03-31
  - input.
  - tipos de variables.
  - operadores.

## Clase 2021-04-07
  - condicional.
  - listas.
  - range

## Clase 2021-04-14
  - slice listas
  - diccionario

## Clase 2021-04-21
  - tuplas
  - funciones

## Clase 2021-04-28
  - objetos

## Clase 2021-05-05
  - objetos. enum.

## Clase 2021-05-12


## Clase 2021-05-19
  - Funciones lambda
  - Funciones sobre listas: map, zip, filter, reduce, enumerate.

## Clase 2021-05-26
  - Archivos
