# f(x) = 2x + 10

f = lambda x: 2 * x + 10
g = lambda x, y: 2*x+5*y-3

y = f(4)
print(y)

z = g(3, 5)
print(z)

# x ** n

def construir_fc_potencia(n):
  return lambda x: x ** n


h1 = construir_fc_potencia(2)
print(h1(5))
print(h1(2))

h2 = construir_fc_potencia(4)
print(h2(4))
print(h2(2))

no_retorna = lambda m: print(m)
no_retorna('hola')


