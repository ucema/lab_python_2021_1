import random

random.seed(0)

valores = []
valores2 = []
valores3 = []
for x in range(10):
    valores.append(random.randrange(0, 20))
    valores2.append(random.randrange(0, 20))
    valores3.append(random.randrange(0, 20))

print(valores)

# map -> a partir de una lista, le aplica a cada elemento una funcion creando otra lista

superficies_v1 = []
for n in valores:
    superficies_v1.append(n * n)

print(superficies_v1)

def calcular_superficie(n):
    return n * n

superficies_v2 = list(map(calcular_superficie, valores))
print(superficies_v2)

#--------------------------------------------------------------------------------------

class Factura():
    numero = 0
    fecha = ''
    cantidades = []
    precios = []

    def __init__(self, numero=0, fecha='', cantidades=[], precios=[]):
        self.numero = numero
        self.fecha = fecha
        self.cantidades = cantidades
        self.precios = precios

def calcular_total(factura):
    total = 0
    for i in range(len(factura.cantidades)):
        total = total + factura.cantidades[i] * factura.precios[i]

    return total

f1 = Factura(1, '20200901', [2, 1, 2], [12.4, 35.1, 21.5])
f2 = Factura(2, '20200907', [1, 1], [35.1, 21.5])
f3 = Factura(3, '20200824', [10, 5], [20.99, 49.99])

fcs = [f1, f2, f3]

totales = list(map(calcular_total, fcs))
print(totales)

#--------------------------------------------------------------------------------------

superficies_v3 = list(map(lambda n: n * n, valores))
print(superficies_v3)

#--------------------------------------------------------------------------------------

print('-' * 50)
valores3 = valores3[:5]

print(valores)
print(valores2)
print(valores3)

por_dos = list(map(lambda a, b, c: a+b+c, valores, valores2, valores3))
print(por_dos)
