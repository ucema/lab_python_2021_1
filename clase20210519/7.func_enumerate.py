nombres = ['uno', 'dos', 'tres', 'cuatro']

for i in range(len(nombres)):
    valor = nombres[i]

print(list(enumerate(nombres)))

'''
La funcion 'enumerate' genera una coleccion de tuplas con los valores de la coleccion que recibe 
como parametros.
'''
for indice, valor in enumerate(nombres):
    print(indice, valor)

print('-' * 50)

'''
A menos que se indique lo contrario, la funcion 'enumerate' comienza a numerar desde 0,
si se necesita que comience en otro valor, se agregar un argumento mas: enumerate(nombres, 5)
'''
for indice, valor in enumerate(nombres, 5):
    print(indice, valor)


print('-' * 50)

'''
La funcion 'all' es una funcion booleana que devuelve True si todos los elementos 
de la coleccion son True, y False caso contrario. (es como un gran AND)
'''
valores = [True, False, True, True]
p = all(valores)
print(p)


'''
La funcion 'any' es una funcion booleana que devuelve True si al menos uno de los elementos 
de la coleccion es True, y False caso contrario. (es como un gran OR)
'''
q = any(valores)
print(q)
