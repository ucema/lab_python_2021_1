import random
from functools import reduce

# reduce -> 

random.seed(0)

valores = []
for x in range(5):
    valores.append(random.randrange(0, 20))

print(valores)


datos = reduce(lambda resultado, elemento: resultado + elemento, valores)
print(datos)

def sumar(resultado, elemento):
    print(resultado, elemento)
    return resultado + elemento

datos = reduce(sumar, valores)
print(datos)


nombres = ['uno', 'dos', 'tres', 'cuatro']

todos = reduce(lambda res, ele: res + '-' + ele, nombres)
print(todos)
