import random

# zip -> unifica 2 o mas colecciones en una

random.seed(0)

valores = []
valores2 = []
valores3 = []
for x in range(5):
    valores.append(random.randrange(0, 20))
    valores2.append(random.randrange(0, 20))
    valores3.append(random.randrange(0, 20))

print(valores)
print(valores2)
print(valores3)

datos = list(zip(valores, valores2, valores3))
print(datos)

print('-' * 50)

valores3 = valores3[:2]
valores4 = ['uno', 'dos', 'tres', 'cuatro']

print(valores)
print(valores2)
print(valores3)
print(valores4)

datos = list(zip(valores, valores2, valores3, valores4))
print(datos)

print('-' * 50)

def funcion1():
    print('funcion1')
    return random.randint(0, 50)

def funcion2():
    print('funcion2')
    return random.randint(0, 50)

def funcion3():
    print('funcion3')
    return random.randint(0, 50)

funciones = [funcion1, funcion2, funcion3]
datos = list(zip(valores, funciones))
print(datos)

print(datos[0][1]())

nro_funcion = int(input('Que funcion quiere ejecutar (0,1,2): '))
y = funciones[nro_funcion]()
print(y)
