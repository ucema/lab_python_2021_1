import random

# filter -> filtrar los datos de una coleccion, segun una funcion booleana

random.seed(0)

valores = []
for x in range(30):
    valores.append(random.randrange(0, 20))

print(valores)


mayores_10 = list(filter(lambda n: n > 10, valores))
print(mayores_10)

# La suma de todos los numeros pares en la lista 'valores'

suma = sum(filter(lambda n: n % 2 == 0, valores))
print(suma)

# Generar una lista con todos los pares, donde quede el valor de la division por 2
# [1,4,3,6] ---   [2,3]

print('-' * 50)
print(list(filter(lambda n: n % 2 == 0, valores)))

divididos = list(map(lambda n: n // 2, filter(lambda n: n % 2 == 0, valores)))
print(divididos)


print('-' * 50)

class Factura():
    numero = 0
    fecha = ''
    cantidades = []
    precios = []

    def __init__(self, numero=0, fecha='', cantidades=[], precios=[]):
        self.numero = numero
        self.fecha = fecha
        self.cantidades = cantidades
        self.precios = precios

def listar_facturas(lista):
    for fc in lista:
        print(fc.numero, '\t', fc.fecha, '\t', fc.cantidades, '\t', fc.precios, '\t', calcular_total(fc))

def calcular_total(factura):
    total = 0
    for i in range(len(factura.cantidades)):
        total = total + factura.cantidades[i] * factura.precios[i]

    return total

f1 = Factura(1, '20200901', [2, 1, 2], [12.4, 35.1, 21.5])
f2 = Factura(40, '20200907', [1, 1], [35.1, 21.5])
f3 = Factura(3, '20200824', [10, 5], [20.99, 49.99])

fcs = [f1, f2, f3]

fcs_numero_mayor_5 = filter(lambda f: f.numero > 5 and f.fecha < '2021', fcs)
listar_facturas(fcs_numero_mayor_5)

print('-' * 50)

fcs_total = filter(lambda f: calcular_total(f) > 100, fcs)
listar_facturas(fcs_total)
