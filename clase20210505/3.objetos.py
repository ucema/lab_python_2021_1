
class Persona():
    nombre = ''
    edad = 0
    altura = 0

    def __gt__(self, value):      # greater than / mayor que. Devuelve True/False
        return  self.edad > value.edad

    def __ge__(self, value):      # greater or equal
        if self.edad == value.edad:
            return self.altura > value.altura

        return self.edad > value.edad

    def __lt__(self, value):      # less than
        return  self.edad < value.edad

    def __le__(self, value):      # less or equal
        return self.edad <= value.edad

    def __eq__(self, value):
        return self.edad == value.edad

    def __ne__(self, value):
        return self.edad != value.edad



a = Persona()
a.edad = 10

b = Persona()
b.edad = 5

c = Persona()
c.edad =19

if a > b:         # if a.__gt__(b):
    print('a es mayor que b')

if a <= c:
    print('a es menor or igual que c')

