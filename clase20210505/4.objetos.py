
class Persona():
    nombre = ''
    edad = 0
    altura = 0.0

    def __init__(self, nombre, edad, altura):
        self.nombre = nombre
        self.edad = edad
        self.altura = altura

    def __str__(self):
        return f'Soy {self.nombre} tengo {self.edad} y mido {self.altura:.2f}'        # Placeholders o huecos
        
        # return 'Soy {} tengo {} y mido {:.2f}'.format(self.nombre, self.edad, self.altura)
        
        # return 'Soy {0} tengo {1} y mido {2:.2f}, te dije que tengo {1} años?'.format(self.nombre, self.edad, self.altura)

        # return 'Soy ' + self.nombre + ' tengo ' + str(self.edad) + ' y mido ' + str(self.altura) + '.'


a = Persona('uno', 11, 110.33333333333333)
b = Persona('dos', 12, 120.75)
c = Persona('tres', 13, 130.2)

print(a)
print(b)
print(c)

s = 'Valor: {0:.2f} - {1} - {2} - {1}'.format(1 / 3, 5, 'abc')
print(s)

print('*' * 50)

a.peso = 70
print(a.peso)
# print(b.peso)   El objeto b no tiene el atributo peso

print('*' * 50)

# nombre_atributo = input('Atributo a modificar: ')
# valor = input('Ingrese valor: ')
# a.__setattr__(nombre_atributo, valor)

# print(a.nombre)
# print(a.peso)
# print(a.iq)

print('*' * 50)

nombre_atributo = input('Atributo a consultar: ')
valor = a.__getattribute__(nombre_atributo)
print(valor)



a.__at
