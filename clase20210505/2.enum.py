from enum import Enum

class Colores(Enum):
    RED = 'FF0000'
    GREEN = '00FF00'
    BLUE = '0000FF'
    YELLOW = '00FFFF'


for c in Colores:
    print(c)