'''
TipoDocumento:
  DNI = 98
  CUIT = 96
  CUIL = 95
  PASAPORTE = 10
'''

from enum import Enum   # importando la clase Enum


class TipoDocumento(Enum):
    DNI = 98
    CUIT = 96
    CUIL = 95
    PASAPORTE = 10
    CEDULA_PFA = 11

    def __str__(self):
        return 'Tipo de Documento ' + self.name + ' y su valor es ' + str(self.value)

    def validar(self, numero):
        if self == TipoDocumento.DNI:
            if len(str(numero)) == 8:
                print('DNI Valido')
            else:
                print('DNI No Valido')

        elif self == TipoDocumento.CUIT:
            if len(str(numero)) == 11:
                print('CUIT Valido')
            else:
                print('CUIT No Valido')


print(TipoDocumento.DNI)

print(TipoDocumento(98))

if TipoDocumento.DNI == TipoDocumento(98):
    print('Es igual')

TipoDocumento.DNI.validar(12345678)
TipoDocumento.CUIT.validar(20112223334)


for td in TipoDocumento:
    print(td)
    print(td.value)


x = int(input('Ingrese tipo documento: '))
if TipoDocumento.DNI.value == x:
    print('Es igual')