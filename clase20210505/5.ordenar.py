
class Persona():
    nombre = ''
    edad = 0
    altura = 0.0

    def __init__(self, nombre, edad, altura):
        self.nombre = nombre
        self.edad = edad
        self.altura = altura

    def __str__(self):
        return f'Soy {self.nombre} tengo {self.edad} y mido {self.altura:.2f}'
  
    def __lt__(self, value):
        return self.nombre < value.nombre


a = Persona('uno', 11, 110.33333333333333)
b = Persona('dos', 12, 120.75)
c = Persona('tres', 13, 130.2)
d = Persona('cuatro', 14, 140.12)

numeros = [54,12,8,32,12,65]
numeros.sort()

print(numeros)

personas = [a, b, c, d]
personas.sort()

for p in personas:
    print(p)

print('*' * 50)
personas.sort(reverse=True)
for p in personas:
    print(p)

print('*' * 50)

personas.sort()
personas.reverse()
