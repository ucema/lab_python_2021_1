from operator import attrgetter


class Factura():
    numero = 0
    fecha = ''
    cantidades = []
    precios = []

    def __init__(self, numero=0, fecha='', cantidades=[], precios=[]):
        self.numero = numero
        self.fecha = fecha
        self.cantidades = cantidades
        self.precios = precios

    def __lt__(self, otro):
        return self.numero < otro.numero


def listar_facturas(lista):
    for fc in lista:
        print(fc.numero, '\t', fc.fecha, '\t', fc.cantidades, '\t', fc.precios, '\t', calcular_total(fc))

def calcular_total(factura):
    total = 0
    for i in range(len(factura.cantidades)):
        total = total + factura.cantidades[i] * factura.precios[i]

    return total

f1 = Factura(1, '20200901', [2, 1, 2], [12.4, 35.1, 21.5])
f2 = Factura(2, '20200907', [1, 1], [35.1, 21.5])
f3 = Factura(3, '20200824', [10, 5], [20.99, 49.99])

fcs = [f1, f2, f3]
listar_facturas(fcs)

print('*' * 50)
fcs.sort(key=calcular_total)
listar_facturas(fcs)

print('*' * 50)

fcs.sort(key=attrgetter('fecha'))
listar_facturas(fcs)
