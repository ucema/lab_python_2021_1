'''
Un objeto: es una cosa del mundo "real"
  - Tangibles: Auto, Gato, Persona, Mesa
  - No tangibles, conceptuales: Cuenta contable, Asiento Contable, Factura

Prog. Orientada a Objetos
  - Proceso de abstracción
     - Caracteristicas
     - Comportamiento

Gato: color_pelo, edad, correr, dormir, morder

Memoria

Clase (class): Es una especie de plano (codificado) para crear los objetos en memoria
'''

class Gato():
    # Atributos de la clase     -->    las caracteristicas de los objetos
    color_pelo = ''
    edad = 0

    # Metodos de la clase (funciones)  -->  el comportamiento de los objetos
    def correr(self):
        print('Gato corriendo')

    def dormir(self, horas):
        print('Durmiendo ', horas, ' horas.')

pipi = Gato()  # Construye un objeto del tipo Gato
pipi.color_pelo = 'Negro'
pipi.edad = 7

pepe = Gato()
pepe.color_pelo = 'Blanco'
pepe.edad = 3

print(pipi.color_pelo)
print(pepe.color_pelo)

pipi.correr()
pipi.dormir(2)
