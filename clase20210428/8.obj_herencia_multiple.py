class ClaseA():
    def metodo1(self):
        print('ClaseA.metodo1()')

class ClaseB(ClaseA):
    def metodo1(self):
        print('ClaseB.metodo1()')

class ClaseC(ClaseA):
    def metodo1(self):
        print('ClaseC.metodo1()')

class ClaseD(ClaseC, ClaseB):
    pass


o = ClaseD()
o.metodo1()
