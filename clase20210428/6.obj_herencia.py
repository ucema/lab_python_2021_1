# Generalizacion: Gato, Tigre, Leon  --es un--> Felino
#                                                    ---es un----> Mamifero   --es un-->  Animal
#                 Perro, Hiena, Lobo --es un--> Canino         


class Felino():
    tipo_pelo = ''

    def ronronear(self):
        print('Rrrrrrrr')


class Gato(Felino):
    nombre = ''

    def __init__(self, nombre):
        self.nombre = nombre

    def ronronear(self):
        super().ronronear()
        print("Grrr")


gato1 = Gato('Uno')
gato1.ronronear()