class Mamifero():

    def alimentar(self):
        print('Tomando leche')


class Felino():
    tipo_pelo = ''

    def ronronear(self):
        print('Rrrrrrrr')


class Gato(Felino, Mamifero):   # Herencia multiple, heredar de mas de una clase.
    nombre = ''

    def __init__(self, nombre):
        self.nombre = nombre


gato1 = Gato('Uno')
gato1.ronronear()
gato1.alimentar()
