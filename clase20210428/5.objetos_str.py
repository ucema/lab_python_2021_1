# Herencia: Recibir atributos y metodos de una clase "superior" o
#     superclase o clase base o clase padre

# Todas las clases heredan una superclase: Object

class Gato():
    color_pelo = ''
    edad = 0
    peso = 0
    nombre = ''

    def __init__(self, nombre, peso=200):
        self.nombre = nombre
        self.peso = peso

    def __str__(self):    # Cambiar el comportamiento de un metodo heredado (sobreescritura)
        s = 'Me llamo ' + self.nombre
        s = s + ' y tengo ' + str(self.edad)
        return s


gato1 = Gato('Uno')

x = str(gato1)
print(x)
