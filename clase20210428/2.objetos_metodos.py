class Gato():
    # Atributos de la clase     -->    las caracteristicas de los objetos
    color_pelo = ''
    edad = 0
    metros_corridos = 0

    # Metodos de la clase (funciones)  -->  el comportamiento de los objetos
    def correr(self, metros=1):
        print('Gato corriendo')
        self.metros_corridos = self.metros_corridos + metros

    def dormir(self, horas):
        print('Durmiendo ', horas, ' horas.')


pipi = Gato()  # Construye un objeto del tipo Gato
pipi.color_pelo = 'Negro'
pipi.edad = 7

pepe = Gato()
pepe.color_pelo = 'Blanco'
pepe.edad = 3

print(pipi.color_pelo)
print(pepe.color_pelo)

pipi.correr()
pipi.correr(5)
pipi.correr(2)

pipi.dormir(2)
print(pipi.metros_corridos)