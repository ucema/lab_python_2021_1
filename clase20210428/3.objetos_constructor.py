
# Crear una clase Gato, que al crearla se le asigne un nombre

class Gato():
    color_pelo = ''       # Variables de estado o atributos
    edad = 0
    peso = 0
    nombre = ''

    # Metodo constructor
    # Metodo constructor por default: No hace nada
    def __init__(self, nombre, peso=200):
        print('Construyendo:', nombre)
        self.nombre = nombre
        self.peso = peso


print('Antes de construir')
gatito = Gato('Uno')
print('Despues de construir')

print(gatito.nombre, gatito.peso)

gato2 = Gato('Dos', 450)
print(gato2.nombre, gato2.peso)
