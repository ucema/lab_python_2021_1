class Perro():
    color_pelo = ''
    edad = 0
    peso = 0
    nombre = ''

    def __init__(self, nombre, peso=200):
        self.nombre = nombre
        self.peso = peso

    def hacer_necesidades(self):
        print('Hago mis cosas', self.nombre)


class Paseador():
    nombre = ''
    perros = []

    def __init__(self, nombre):
        self.nombre = nombre

    def pasear_perros(self):
        for p in self.perros:
            p.hacer_necesidades()


perro1 = Perro('Uno')
perro2 = Perro('Dos')
perro3 = Perro('Tres')
perro4 = Perro('Cuatro')

pepe = Paseador('Pepe')
pepe.perros.append(perro1)
pepe.perros.append(perro2)
pepe.perros.append(perro3)
pepe.perros.append(perro4)

pepe.pasear_perros()

# perro1.hacer_necesidades()

# pepe.perros[0].hacer_necesidades()
# print(pepe)
# print(perro1)
# print(perro2)
# print(perro3)