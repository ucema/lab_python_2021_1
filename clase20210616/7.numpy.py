import numpy as np

ran = np.random.default_rng(0)

a = (ran.random((2,3)) * 20).astype('int32')
b = (ran.random((2,4)) * 20).astype('int32')
c = (ran.random((2,2)) * 20).astype('int32')

print(a,'\n\n', b,'\n\n', c)

h = np.hstack((a,b,c))  # Poner una al lado de la otra
print(h)

a = (ran.random((3, 3)) * 20).astype('int32')
b = (ran.random((4, 3)) * 20).astype('int32')
c = (ran.random((2, 3)) * 20).astype('int32')

print()
print(a,'\n\n', b,'\n\n', c)

v = np.vstack((a,b,c))
print(v)

print('-' * 50)

s1 = np.hsplit(h, 3)
print(s1[0])
print(s1[1])
print(s1[2])

print('-' * 50)
s1 = np.hsplit(h, [5,8])
print(s1[0])
print(s1[1])
print(s1[2])