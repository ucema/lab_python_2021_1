import random

# x = (a>b?a:b)
a = random.randint(0,50)
b = random.randint(0,50)

x = a if a>b else b

# if a>b:
#   x = a
# else:
#   x = b

print(a,b,x)

# numeros = []
# for x in range(10):
#   numeros.append(random.randint(0,50))

numeros = [ n for n in range(10) ]
print(numeros)

numeros = [ n*2 for n in range(10) ]
print(numeros)


numeros = [ random.randint(0,50) for n in range(10) ]
print(numeros)

diccionario = {
  1: 'uno',
  2: 'dos',
  3: 'tres',
  4: 'cuatro'
}
d = [ str(k)+'-'+v  for k,v in diccionario.items() ]
print(d)

d = { n: n for n in numeros }
print(d)


print('-' * 50)


class Factura():
    numero = 0
    fecha = ''
    cantidades = []
    precios = []

    def __init__(self, numero=0, fecha='', cantidades=[], precios=[]):
        self.numero = numero
        self.fecha = fecha
        self.cantidades = cantidades
        self.precios = precios

    def __lt__(self, otro):
        return self.numero < otro.numero

def calcular_total(factura):
    total = 0
    for i in range(len(factura.cantidades)):
        total = total + factura.cantidades[i] * factura.precios[i]

    return total

f1 = Factura(1, '20200901', [2, 1, 2], [12.4, 35.1, 21.5])
f2 = Factura(2, '20200907', [1, 1], [35.1, 21.5])
f3 = Factura(3, '20200824', [10, 5], [20.99, 49.99])

fcs = [f1, f2, f3]

totales = { f.numero: calcular_total(f) for f in fcs }
print(totales)


print('-' * 50)

t = tuple( n for n in numeros)
print(t)
