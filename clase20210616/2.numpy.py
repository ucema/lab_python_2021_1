import numpy as np
import random

numeros = np.array([ random.randint(0, 10) for n in range(16) ]) #, dtype='complex')
print(numeros)

numeros2 = np.array([ random.randint(0, 10) for n in range(16) ])
print(numeros2)

matriz = numeros.reshape(4, 4)
print(matriz)

matriz2 = numeros2.reshape(4, 4)
print(matriz2)

suma_matrices = matriz + matriz2
print(suma_matrices)

print('-' * 50)

vector1 = np.array([2,6,3,5])

r = matriz * vector1
print(r)

z = np.zeros((4, 4))
print(z)

print('-' * 50)

x_dist = np.linspace(0, 10, 20)
print(x_dist)
