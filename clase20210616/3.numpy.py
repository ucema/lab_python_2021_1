import numpy as np
import random

numeros = np.array([ random.randint(0, 10) for n in range(16) ]) #, dtype='complex')
print(numeros)

print('Suma:       ', numeros.sum())
print('Min:        ', numeros.min())
print('Max:        ', numeros.max())
print('Promedio:   ', np.average(numeros))
print('Desvio Std: ', numeros.std())
print('Mediana:    ', np.median(numeros))

print('-' * 50)

matriz = numeros.reshape(4,4)
print(matriz)

suma_filas = matriz.sum(axis=1)   # 1: Filas, 0: Columnas
print(suma_filas)

t_matriz = matriz.transpose()
print(t_matriz)

t_suma_filas = suma_filas.transpose()
print(t_suma_filas)

deter = np.linalg.det(matriz)
print(deter)
