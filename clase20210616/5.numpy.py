import numpy as np
import random

numeros1 = np.array([ random.randint(0, 10) for n in range(10) ])
print(numeros1)

numeros2 = np.array([ random.randint(0, 10) for n in range(10) ])
print(numeros2)

mascara = np.array([ random.randint(0,1)==1 for n in range(10) ])
print(mascara)

r1 = np.where(mascara, numeros1, numeros2)
print(r1)

print(numeros1 > numeros2)
r2 = np.where(numeros1 > numeros2, numeros1, numeros2)
print(r2)

print()
print((numeros1 < 5) & (numeros1>2))

print()
r3 = np.argwhere(numeros1 > 5)
print(r3)

im = numeros1.argmax()
print(im)
print(numeros1[im])

ran = np.random.default_rng(0)
m = (ran.random((7,5)) * 100).astype('int32')
print(m)

im = m.argmax()
print(im)
print(m.ravel()[im])
