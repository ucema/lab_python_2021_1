import numpy as np
import random

numeros = np.array([ random.randint(0, 10) for n in range(16) ]) #, dtype='complex')
print(numeros)

a = np.arange(10) ** 3
print(a)

def f(x, y):
    return 5*x+2*y

b = np.fromfunction(f, (5,3), dtype=int)
print(b)

for fila in b:
  print(fila)

for elem in b.flat:
  print(elem, end='  ')

print()

c = b.ravel()
print(c)

b.resize(3,5)
print(b)

