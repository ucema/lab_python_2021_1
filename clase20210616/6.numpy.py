import numpy as np

ran = np.random.default_rng(0)
m = (ran.random((7,5)) * 100)
print(m)

a = np.floor(m).astype('int32')
print(a)

b = np.ceil(m).astype('int32')
print(b)

t = a.T
print(t)

print('-' * 50)

m = (ran.random((4,3)) * 20).astype('int32')
print(m)
m[2,2] = 100
print(m)

v = m.view()
v[0,1]=101

print(m.data)
print(v.data)

print('-' * 50)

c = m.copy()
print(c)
c[2,0]=999
print(m)
print(c)