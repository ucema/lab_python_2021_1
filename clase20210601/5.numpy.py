import numpy as np

vectorA = np.array([1,2,3,4,5,6])
print(vectorA)
print(type(vectorA))

print(vectorA[0])

r = vectorA * 5
print(r)

vectorB = np.arange(1, 18, 3)
print(vectorB)

s = vectorA + vectorB
print(s)

print('Dimension de s: ', s.ndim)

print('Cantidad elementos: ', s.size)

vectorC = np.arange(0, 25, 1)
print(vectorC)
print(vectorC.ndim, vectorC.size)

matrizC = vectorC.reshape(5, 5)
print(matrizC)

matrizC[1][3] = 111
matrizC[1, 2] = 100
print(matrizC)
print(matrizC.ndim, matrizC.size)
print(matrizC.dtype)
