class MiError(Exception):
    
    def __init__(self, leyenda, numero):
        super(MiError, self).__init__(leyenda)
        self.numero = numero

    def un_metodo(self):
        return numero


print('Comienzo')

try:
    raise MiError('Algo feo pasó', 1234)

except MiError as me:
    print('Se produjo un MiError')
    print(me)
    print(me.numero)

except Exception:
    print('Un error')

print('Fin.')
