class ContabilidadError(Exception):
    pass


class AsientoDesbalanceadoError(ContabilidadError):
    
    def __init__(self, importe_debe, importe_haber):
        super(AsientoDesbalanceadoError, self).__init__()
        self.importe_debe = importe_debe
        self.importe_haber = importe_haber

    def diferencia(self):
        return self.importe_haber - self.importe_debe


class CuentaContableInvalidaError(ContabilidadError):
    pass



debe = float(input('Importe debe: '))
haber = float(input('Importe haber: '))
if debe != haber:
    raise AsientoDesbalanceadoError(debe, haber)
    