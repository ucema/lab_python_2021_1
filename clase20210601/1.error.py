# Excepciones: (Errores)

print('Comienzo')

try:
    x = int(input('Ingrese numero: '))
    print(1 / x)                # Si x es 0, ZeroDivisionError

    yes_no = input('Producir overflow (s/n): ')
    if yes_no == 's':
        j = 5.0                     # OverflowError
        for i in range(1, 1000):
            j = j**i

    archivo1 = open('file1.txt', 'r')
    texto = archivo1.read()
    archivo1.close()

    print('Despues del error')

except ZeroDivisionError as e:
    print('----------------')
    print(e.args)
    print('Ocurrio un error div por cero.')
except OverflowError:
    print('----------------')
    print('Ocurrio un error de overflow.')
except ArithmeticError:
    print('----------------')
    print('Ocurrio un error aritmetico.')
except FileNotFoundError:
    print('No se ha encontrado el archivo')
except Exception as ex:
    print(type(ex))
    print('un error ha pasado')

print('Fin.')
