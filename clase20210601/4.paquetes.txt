Paquetes


numpy: algebra (vectores y matrices)


numpy==1.19.2     La version con la que estoy trabajando

numpy==1.19.4     Hubo cambio en 'revision' en general: no pasa nada

numpy==1.20.4     Hubo cambio en 'minor' OJO, pueden haber cambios que rompan codigo

numpy==2.1.2      Hubo cambio en 'major' OJO QUE ROMPE TODO

major.minor.revision


'Entornos Virtuales'
pip install virtualenv 

Windows:    pip
Linux/Mac:  pip3 

Creamos un entorno virutal:    virtualenv env

Activar el entorno virtual:
---------------------------

Bash en Windows:              source env/Scripts/activate

Command (cmd) en windows:     cd env/Scripts
                              activate.bat

Linux/Mac:                    source env/bin/activate

Desactivar el entorno virtual:    deactivate

pip install numpy
pip install flask

Guardo el listado de paquetes (con su versiones) en un archivo
pip freeze > requeriments.txt


Instala los paquetes con la version correspondiente del archivo:
pip install -r requeriments.txt 
