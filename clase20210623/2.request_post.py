import requests, json


class Persona():
    nombre = ''
    edad = 0
    altura = 0

    def __init__(self, nombre, edad, altura):
      self.nombre = nombre
      self.edad = edad
      self.altura = altura
        

a = Persona('Pipi', 100, 180)
b = Persona('Pepe', 50, 173)

ps = [a, b]

resp_post = requests.post('https://httpbin.org/post', data={'personas': [p.__dict__ for p in ps]})
if resp_post.ok:
  respuesta = resp_post.json()
  print(respuesta)

print('*' * 50)

s = json.dumps([p.__dict__ for p in ps])
print(ps)
print(s)

resp_post = requests.post('https://httpbin.org/post', data=s)
respuesta = resp_post.json()
print(respuesta)
