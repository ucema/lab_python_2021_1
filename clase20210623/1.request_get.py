'''
HTTP: Hyper Text Transfer Protocol   (TEXTO)

REST:
   GET:  Obtener un recurso
   POST: Creacion de un rescurso
   DELETE: Eliminacion de un recurso
   PUT: Modificar  (envia el recurso completo)
   PATCH: Modificar (envia las "partes" modificadas)

   ACTIONS: Ver metodos disponibles al recurso

curl: https://curl.haxx.se/windows/
postman

https://www.google.com?q=python

url friendly: www.amazon.com/computacion/partes/discos/solidos

cliente -----> request -----> servidor
                                 |
                                 |  procesa
                                 |
cliente <------response----------+

https://httpbin.org

'''

# respuesta es Response
# 1xx Informaciones
# 2xx Correcto
# 3xx Redireccionamientos
# 4xx: Errores de request
# 5xx: Error del servidor

import requests, json

respuesta = requests.get('https://httpbin.org/get?param1=1234&param2=qwerty')

if respuesta.status_code == 200:
  print('ok')
else:
  print('error')

if respuesta.ok:
  print(respuesta.headers)
  print()

  datos = respuesta.json()
  print('args: ', datos['args'])

print('*' * 50)

respuesta = requests.get('https://swapi.dev/api/people/1')
if respuesta.ok:
  personaje = respuesta.json()
  print(personaje)

  planeta_get = requests.get(personaje['homeworld'])
  planeta = planeta_get.json()
  print(planeta['name'])
