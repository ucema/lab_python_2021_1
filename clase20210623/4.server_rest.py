'''
Flask
FastAPI

Django-rest-framework



Django: ORM-Seguridad-Admin-Web templating....
'''

from flask import Flask, Response, request
import json

app = Flask(__name__)
app.debug = True


# Blueprints de flask


# creo los endpoints
@app.route('/')
def raiz_sitio():
  return 'Este es el inicio'

@app.route('/ruta')
def otra_ruta():
  return 'otra ruta...'

@app.route('/ip')
def consultar_ip():
  print(request.args)
  print(request.remote_addr)
  print(request.args.get('param'))
  
  return 'Su IP es ' + request.remote_addr

@app.route('/amigable/<nombre>')
def soy_amigable(nombre):
  print(nombre)
  return 'Hola ' + nombre



app.run()
